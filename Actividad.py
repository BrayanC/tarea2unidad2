# autor: Brayan Gonzalo Cabrera Cabrera
# email: brayan.cabrera@unl.edu.ec

# Crear un programa en el cual se permita crear una actividad que contenga al menos 3 preguntas de
# selección en base al modelo de clases codificado en el item 1 presentado al final todo lo creado.
# Todo el código del proyecto debe estar en un proyecto en GitLab

datos = []
preguntas = []
opciones = []
respuestas = []


class actividades:
    identificador = " "
    nombre = " "
    num_preguntas = 0

    def __init__(self ,nombre, identificador, num_preguntas):
        self.identificador = nombre
        self.nombre = identificador
        self.num_preguntas = num_preguntas

    def datos_personales(self):
        nombre = input(" Ingrese su nombre: ")
        datos.append(nombre)
        identificador = input(" Ingrese N° de cedula: ")
        datos.append(identificador)
        print(datos)

    def generar_preguntas(self):
        for i in range(0, num_preguntas):
            pregunta = input(f" Ingrese pregunta {i + 1}: ")
            preguntas.append(pregunta)
            for a in range(1, 4):
                opcion = input(f" Opción {a}: ")
                opciones.append(opcion)
            for r in range(1):
                respuesta = input(" Ingrese respuesta correcta: ")
                respuestas.append(respuesta)

    def presentacion(self):
        print(" \n CUESTIONARIO \n")
        for i in range(0, self.num_preguntas):
            print(f" >>> Pregunta {i + 1}: ", preguntas[i])
            for h in range(0, self.num_preguntas ):
                print(f" opcion{h + 1}:  ", opciones[h])
            print(" Respuesta correcta: ", respuestas[i])

    def __str__(self):
        return f" {self.presentacion()}"


class PreguntaSeleccion(actividades):
    numero = 0
    texto = " "
    respuesta = ""

    def __init__(self, numero, texto, respuesta, nombre, identificador, num_preguntas):
        super().__init__(nombre, identificador, num_preguntas)
        self.numero = numero
        self.texto = texto
        self.respuesta = respuesta


num_preguntas = int(input("Ingrese el numero de preguntas: "))
cuest = actividades("Autor", "Identificador", num_preguntas)
cuest.generar_preguntas()
cuest.presentacion()